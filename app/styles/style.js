import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10
    },
    body: {
        flex: 1,
        justifyContent: 'center',
    },
    welcome: {
        textAlign: 'center'
    }
});