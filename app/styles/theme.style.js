export default {
  BUTTON_COLOR: '#1abc8c',
  // BACKGROUND_COLOR: '#1abc9c',
  BACKGROUND_COLOR: 'blue',
  centerRowAligment:{
    flex: 1,
    backgroundColor:"#a22a5f",
    flexDirection: 'row',
    alignItems:"center"
},
centerColumnAligment:{
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
}
};
