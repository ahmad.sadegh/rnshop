const books = [
  {
    id: '1',
    title: 'شیر روزانه دامدارن',
    author: 'فروشگاه دارچین',
    category: 'لبنیات',
    publicationdate: '2017 by John Wiley & Sons',
    introduction:
      'شیر 1 لیتری روزانه',
    picture: require('./assets/images/super/shir.jpg'),
    cost: 25,
  },
  {
    id: '2',
    title: 'خامه شکلاتی',
    author: 'فروشگاه دارچین',
    category: 'لبنیات',
    publicationdate: '2015-5-5',
    introduction:
      'خامه 100 گرمی روزانه',
    picture: require('./assets/images/super/khame.jpg'),
    cost: 35.99,
  },
  {
    id: '3',
    title: 'روغن',
    author: 'فروشگاه دارچین',
    category: 'خواربار',
    publicationdate: '2016-5-10',
    introduction: 'روغن 1 لیتر ی مایع ',
    picture: require('./assets/images/super/roghan.jpg'),
    cost: 25.99,
  },
  {
    id: '4',
    title: 'Pro Git',
    author: 'فروشگاه دارچین',
    category: 'لبنیات',
    publicationdate: '2016-5-10',
    introduction: 'پنیر لبنه ',
    picture: require('./assets/images/super/panir.jpg'),
    cost: 45.99,
  },
  {
    id: '5',
    title: 'ماکارونی',
    author: 'فروشگاه دارچین',
    category: 'خواربار',
    publicationdate: '2016-7-10',
    introduction:
      'ماکارونی ساده ',
    picture: require('./assets/images/super/makaroni.jpg'),
    cost: 20.99,
  },
  {
    id: '6',
    title: 'رب گوجه',
    author: 'فروشگاه دارچین',
    category: 'خواربار',    
    publicationdate: '2013-5-10',
    introduction:
      'رب گوجه 1 کیلویی',
    picture: require('./assets/images/super/rob.jpg'),
    cost: 22,
  },  
];

export const getProducts = () => {
  return books;
};
