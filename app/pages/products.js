import React, { useState, useEffect } from "react";

import { View, StyleSheet, FlatList, Text } from 'react-native';
import { connect } from 'react-redux';
import Product from '../components/Product.component';
import { addToCart } from '../redux/actions/cartActions';
import { fetchProducts } from '../redux/actions/productAction';
import Logo from '../components/Logo.component';
import Cart from '../components/Cart.component';
import themes from '../styles/theme.style';
import mainStyle from '../styles/style';
import { useSelector } from "react-redux";



const Products = (props) => {
  const { StoreProducts, navigation } = props;//StoreProduct az map state to props miad
  const ui = useSelector(state => state.products);//products comes from combine reducer object
  const [products, setProducts] = useState([]);
  const [cat, setCat] = useState()
  useEffect(() => {
    let noasync;
    if (props.route.params) {
      setCat(props.route.params.category);
      noasync=props.route.params.category;
    }
    const pr = props.fetchProducts(noasync);
    setProducts(pr);
    // setProducts(ui.items);// in kar nemikone chon store statehash async update mishe ba takhir
  }, []
  )
  //function zir dar product.component call mishe 
  //khodesh mire addToCart ro az redux call mikone
  const addItemsToCart = product => {
    props.addToCart(product);
  };

  return (
    <View style={mainStyle.container}>
      <View style={mainStyle.body}>
        <Text>{cat}</Text>
        <FlatList
          data={products} //in az hame behtar chon momkene taghyirat dige ham dar ebteda bedim
          // data={ui.items} //inam mishe chon store dar useeffect update shod
          // data={StoreProducts} //inam mishe
          renderItem={({ item }) => (
            <Product
              item={item}
              addItemsToCart={addItemsToCart}
              product={item}
            />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={() => (
            <View style={{ height: 0.5, backgroundColor: '#34495e90' }} />
          )}
        />
      </View>
    </View>
  )

}

const mapStateToProps = state => ({
  StoreProducts: state.products.items,//products dar combinereducer object tarif shode yeki az reducerhashe
});
export default connect(
  mapStateToProps,
  { addToCart, fetchProducts },
)(Products);

