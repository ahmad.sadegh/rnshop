import React, { Component, useMemo, useState } from 'react';
// import { AsyncStorage } from 'react-native';
import { Container, Content, List, ListItem, Text, Left, Right, Icon, Button } from 'native-base';
const Categories = (props) => {
  const { navigation } = props;
  const [loading, setLoading] = useState(true);

  useMemo(async () => {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      // 'Ionicons': require('@expo/vector-icons/fonts/Ionicons.ttf'),
    });
    setLoading(false);
  }, []
  )

  const gotoMahsul = (cat) => { 
    // AsyncStorage.setItem('MyToken', token);
    // AsyncStorage.getItem('MyToken').then(token => {
    navigation.navigate('Products', {
    category: cat,
    // otherParam: 'anything you want here',
  }); }

  return (
    (!loading) &&
    <Container>
      <Content>
        <List>
          <ListItem selected>
            <Left>
              <Button iconRight light onPress={()=>gotoMahsul('لبنیات')}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Right>
              <Text>لبنیات</Text>
            </Right>
          </ListItem>
          <ListItem>
            <Left>
              <Button iconRight light onPress={()=>gotoMahsul('خواربار')}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Right>
              <Text>خواربار</Text>
            </Right>
          </ListItem>
          <ListItem>
            <Left>
              <Button iconRight light onPress={()=>gotoMahsul('شوینده')}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Right>
              <Text>شوینده</Text>
            </Right>
          </ListItem>
          <Button primary style={{ justifyContent: 'center' }}
            onPress={() => navigation.navigate('Products')}>
            <Text>
              همه محصولات
                </Text>
          </Button>
        </List>
      </Content>
    </Container>

  );
}

export default Categories;