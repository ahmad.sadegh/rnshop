import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Product from '../components/Product.component';
import { addToCart } from '../redux/actions/cartActions';
import { fetchProducts } from '../redux/actions/productAction';
import Logo from '../components/Logo.component';
import Cart from '../components/Cart.component';
import themes from '../styles/theme.style';
import mainStyle from '../styles/style';

// const Products= (props)=> {
class Products extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: themes.BACKGROUND_COLOR,
        // paddingHorizontal: '10px !important',
      },
      headerTintColor: '#fff',
      headerTitle: 'Products',
      // headerLeft: <Logo navigation={navigation} />,
      headerLeft: () => <Logo navigation={navigation} />, //navigation khodemun ro be logo frestadim tabetune switch page kone dar unja
      // headerRight: <Cart navigation={navigation} />,
      headerRight: () => <Cart navigation={navigation} />,
    };
  };
  constructor(props) {
    super(props);
  }
  componentWillMount = () => {
    this.props.fetchProducts();
  };
  addItemsToCart = product => {
    this.props.addToCart(product);
  };
  render() {
    const { products, navigation } = this.props;
    return (
      // <View style={styles.container}>
       <View style={mainStyle.container}>
        {/* <View style={styles.body}> */}
        <View style={mainStyle.body}>
          <FlatList
            data={products}
            renderItem={({ item }) => (
              <Product
                item={item}
                addItemsToCart={this.addItemsToCart}
                product={item}
              />
            )}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={() => (
              <View style={{ height: 0.5, backgroundColor: '#34495e90' }} />
            )}
          />
        </View>
      </View>
    );
  }
}
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   body: {
//     flex: 1,
//     justifyContent: 'center',
//   },
// });
const mapStateToProps = state => ({
  products: state.products.items,
});
export default connect(
  mapStateToProps,
  { addToCart, fetchProducts },
)(Products);
