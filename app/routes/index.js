import React from "react";

// import {createStackNavigator} from 'react-navigation';
// import { createAppContainer } from 'react-navigation';
import { NavigationContainer } from '@react-navigation/native';

// import { createStackNavigator } from 'react-navigation-stack';

import Products from '../pages/products';
import Checkout from '../pages/Checkout';
import Receipt from '../pages/Receipt';
import Categories from '../pages/Categories';
import Logo from '../components/Logo.component';
import CatLogo from '../components/CategoryLogo.component';
import Cart from '../components/Cart.component';
import themes from '../styles/theme.style';
import {Root} from 'native-base';

import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();

function RootStack() {
  return (
    //in Root nabashe Toast NativeBase kar nemikone , kare constext provider ro mikone taghriban
    <Root>
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Categories"
        screenOptions={{ gestureEnabled: false }}
      >
        <Stack.Screen
          name="Products"
          component={Products}
          options={{
            headerStyle: {
              backgroundColor: themes.BACKGROUND_COLOR,
            },
            headerTintColor: '#fff',
            title: 'َAll Products',
            headerLeft: () => <CatLogo />,
            headerRight: () => <Cart />,

          }}
        />
        <Stack.Screen
          name="Categories"
          component={Categories}
          options={{
            headerStyle: {
              backgroundColor: themes.BACKGROUND_COLOR,
            },
            headerTintColor: '#fff',
            title: 'َگروه ها',
            headerLeft: () => <Cart />,
          }}
        />
        <Stack.Screen
          name="Checkout"
          component={Checkout}
          options={{ title: 'Checkout' }}
        />
        <Stack.Screen
          name="Receipt"
          component={Receipt}
          // initialParams={{ user: 'me' }}
          options={{ title: 'Receipt' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
    </Root>
  );
}
// const RootStack = createStackNavigator(
//   {
//     Products: {screen: Products,
//       navigationOptions: {
//             headerStyle: {
//       backgroundColor: themes.BACKGROUND_COLOR,
//     },
//     headerTintColor: '#fff',
//     headerTitle: 'Products',
//     headerLeft: () => <Logo />, 
//     headerRight: () => <Cart  />,
//       }
//     },
//     Checkout: {screen: Checkout},
//     Receipt: {screen: Receipt},
//   }, {
//   initialRouteName: 'Products',
//   headerMode: 'screen',
// });

// const Route = createAppContainer(RootStack);

// export default Route;
export default RootStack;
