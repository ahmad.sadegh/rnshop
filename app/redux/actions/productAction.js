import {FETCH_PRODUCTS} from './types';
import {getProducts} from '../../data';
export const fetchProducts = (cat) => dispatch => {
  const allProducts=getProducts();
  const products =cat? allProducts.filter(x=>x.category===cat):allProducts;
  dispatch({
    type: FETCH_PRODUCTS,
    payload: products,
  });

  return(products) //return vaghti niaze in dar yek component farkhani she dar yek variable ya state
};
