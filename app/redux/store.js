import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
const middleware = [thunk];
const initialState = {};//mishe ye seri state ham inja gozasht
export default createStore(
  rootReducer,
  initialState,
  applyMiddleware(...middleware),
);
