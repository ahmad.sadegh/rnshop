import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Button, Icon, Text } from 'native-base';
const logoImage = require('../assets/images/eco-logo.png');

const CatLogo = () => {
  const navigation = useNavigation();
  const goCategories = () => {
    navigation.navigate('Categories');
  };
  return (
    // <TouchableOpacity onPress={goCategories}>
    //   <Image source={logoImage} style={{width: 32, height: 32}} />
    // </TouchableOpacity>
    <Button iconRight primary onPress={goCategories}>
      <Icon name='arrow-back' />
      {/* <Text>گروه ها</Text> */}
    </Button>
  );
}
export default CatLogo;
