import React, { useRef, useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Animated } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

const Cart = (props) => {
 
  // const [opacity,setOpacity]=useState(new Animated.Value(1)); //inam mishe
  const opacity=useRef(new Animated.Value(1)).current;//inam mishe
  const navigation = useNavigation();

 
  useEffect( () => {    
      startAnimation();    
}, [props.cartItems])

 function startAnimation() {
    Animated.timing(opacity, {
      toValue: 0,
      duration: 500,
      useNativeDriver :true
    }).start(() => {
      setTimeout(() => {
        endAnimation();
      }, 100);
    });
  }
  function endAnimation() {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 500,
      useNativeDriver :true
    }).start();
  }
 const onPress = () => {
    navigation.navigate('Checkout');

  };
 
    const { cartItems } = props;

    let animatedStyle = { opacity: opacity };
    return (
      <Animated.View style={[styles.container, animatedStyle]}> 
        <TouchableOpacity onPress={onPress}>
          <Text style={styles.cart}>Your cart: {cartItems.length} items</Text>
        </TouchableOpacity>
      </Animated.View>
    );
  
}
const mapStateToProps = state => ({
  cartItems: state.cart.cart,
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cart: {
    color: 'white',
    // color: 'violet',
    fontSize: 14,
  },
});
export default connect(mapStateToProps)(Cart);
