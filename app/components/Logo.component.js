import React, {Component} from 'react';
import {Image, TouchableOpacity} from 'react-native';
import { useNavigation } from '@react-navigation/native';
const logoImage = require('../assets/images/eco-logo.png');

// class Logo extends Component {
const Logo =()=> {
  const navigation = useNavigation();
  const goHome = () => {
    // this.props.navigation.navigate('Products');
    navigation.navigate('Products');
  };
    return (
      <TouchableOpacity onPress={goHome}>
        <Image source={logoImage} style={{width: 32, height: 32}} />
      </TouchableOpacity>
    );
}
export default Logo;
